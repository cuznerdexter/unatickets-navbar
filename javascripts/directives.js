angular.module('navBarAng.directives', [])

.directive("scroll", ['$window', function($window) {
	console.log('Directive: Scroll: loaded');
	return function (scope, elelment, attrs) {
		angular.element($window).bind("scroll", function () {
			if (this.pageYOffset >= 300) {
				//console.log('scroll is over 300px : ');
			}else {
				//console.log('scroll is under 300px :');
			}
			scope.$apply();
		});
	}
}])