angular.module('navBarAng.controllers', [])

.controller('MainCtrl', ['$scope', '$timeout', '$window', '$document',
	function ($scope, $timeout, $window, $document) {
		console.log('MainCtrl:');
		
		$scope.scrollFix;

		$timeout(function () {
			$scope.$on('CartVisible', function(event, args) {	
				console.log('CART VIS: ', args);
				scrollVis = args;
				console.log('scrollVis:', scrollVis);
				$scope.scrollFix = args;
			});
		},0);


		$scope.scrollPos;
		$scope.logoScale;

		$scope.$watch(function () {
			return $window.pageYOffset;
		}, function (scrollY) {
			if(scrollY >= 300) {
				$scope.logoScale = 'una-logo-scaled';
				console.log('SCROLLY: ',scrollY);
			}else
			{
				$scope.logoScale = '';
			}
			
			
		});
		

		

		
}])

.controller('NavCtrl', ['$scope', '$timeout', 
	function ($scope, $timeout) {
		console.log('NavCtrl:');

		var burgerOpen = false;
		var cartOpen = false;

		$scope.mainMenu;
		$scope.burgerMenu;
		$scope.cartMenu;
		$scope.scrollFix;


		$scope.showMenu = function (menu) {

			if(menu == 'cart') {
				if(menu == 'cart' && burgerOpen == true) {
					console.log('swap to cart');
					burgerToCart();
					$scope.$emit('CartVisible', 'stopScroll');
					burgerOpen = false;
					cartOpen = true;
				} else if (menu == 'cart' && cartOpen == true) {
					$scope.cartMenu = 'cartInActive';
					$scope.mainMenu = 'mainCartInActive';
					$scope.$emit('CartVisible', '');
					cartOpen = false;
				}else {
					$scope.cartMenu = 'cartActive';
					$scope.mainMenu = 'mainCartActive';
					$scope.$emit('CartVisible', 'stopScroll');
					cartOpen = true;
				}
			}
			
			if(menu == 'burger') {
				if(menu == 'burger' && cartOpen == true) {
					console.log('swap to burger');
					cartToBurger();
					$scope.$emit('CartVisible', '');
					cartOpen = false;
					burgerOpen = true;
				} else if (menu == 'burger' && burgerOpen == true) {
					//close burger 
					$scope.burgerMenu = 'burgerInActive';
					$scope.mainMenu = 'mainBurgerInActive';
					burgerOpen = false;
				}else {
					$scope.burgerMenu = 'burgerActive';
					$scope.mainMenu = 'mainBurgerActive';
					burgerOpen = true;	
				}
			}

			function burgerToCart() {
				console.log('burgerToCart:');
				$timeout( function () {
					$scope.mainMenu = 'mainBurgerInActive';
					$scope.burgerMenu = 'burgerInActive';
				},0)
				.then(
					$timeout( function () {
						$scope.mainMenu = 'mainCartActive';
						$scope.cartMenu = 'cartActive';
						cartOpen = true;
						burgerOpen = false;
					},500)
				);
			}
			function cartToBurger() {
				console.log('cartToBurger:');
				$timeout( function () {
					$scope.mainMenu = 'mainCartInActive';
					$scope.cartMenu = 'cartInActive';
				},0)
				.then(
					$timeout( function () {
						$scope.mainMenu = 'mainBurgerActive';
						$scope.burgerMenu = 'burgerActive';
						burgerOpen = true;
						cartOpen = false;
					},500)
				);
			}

			
			
			console.log(' burgerOpen:',burgerOpen, ' cartOpen:',cartOpen, ' mainMenu:',$scope.mainMenu, ' burgerMenu:',$scope.burgerMenu, ' cartMenu:', $scope.cartMenu );
			
		};

		
}])