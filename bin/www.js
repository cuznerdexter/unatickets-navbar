//import HTTP module
var http = require('http');
var url = require('url');
var dispatcher = require('httpdispatcher');

//define port to listen to
const PORT=8080;

//function that handles response & request
function handleRequest (req, res) {
	try {
		//log request on console
		console.log(req.url);
		//dispatch
		dispatcher.dispatch(req, res);
		//for all static files - js, css, image etc
		dispatcher.setStatic('resources');
		//sample get request
		dispatcher.onGet("/page1", function(req, res) {
			res.writeHead(200, {'Content-Type': 'text/plain'});
			res.end('Page One');
		});


	} catch (err) {
		console.log(err);
	}
}

//create server
var server = http.createServer(handleRequest);

//start server
server.listen(PORT, function () {
	//callback triggered when successful
	console.log('Server listening on http://localhost:%s', PORT);
});